﻿using BehavioralDesignPatterns.Strategy;
using BehavioralDesignPatterns.Visitors;
using System;
using BehavioralDesignPatterns.Visitors.Figures;

namespace BehavioralDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {

            Point point = new Point(20, 30);

            Square square = new Square(25);

            Circle circle = new Circle(90);

            Context context = new Context();

            Request rXml1 = new Request("XmlSerialize", "VisitorCircle", circle);
            Request rJson1 = new Request("JsonSerialize", "VisitorCircle", circle);


            Request rXml2 = new Request("XmlSerialize", "VisitorPoint", point);
            Request rJson2 = new Request("JsonSerialize", "VisitorPoint", point);

            Request rXml3 = new Request("XmlSerialize", "VisitorSquare", square);
            Request rJson3 = new Request("JsonSerialize", "VisitorSquare", square);

            Console.WriteLine(context.Serialize(rJson1));
            Console.WriteLine(context.Serialize(rXml1));

            Console.WriteLine(context.Serialize(rJson2));
            Console.WriteLine(context.Serialize(rXml2));

            Console.WriteLine(context.Serialize(rJson3));
            Console.WriteLine(context.Serialize(rXml3));

            Console.ReadKey();

        }
    }
}
