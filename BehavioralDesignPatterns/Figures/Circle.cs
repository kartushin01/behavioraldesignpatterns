﻿namespace BehavioralDesignPatterns.Visitors.Figures
{
    class Circle : IShape
    {
        public double Radius { get; set; }

        public Circle(int radius)
        {
            Radius = radius;
        }


    }
}
