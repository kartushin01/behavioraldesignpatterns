﻿namespace BehavioralDesignPatterns.Visitors.Figures
{
    public class Square : IShape
    {
        public int Side { get; set; }

        public Square(int side)
        {
            Side = side;
        }


    }
}
