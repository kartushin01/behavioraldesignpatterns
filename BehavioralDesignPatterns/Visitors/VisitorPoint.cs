﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using BehavioralDesignPatterns.Strategy;
using BehavioralDesignPatterns.Visitors.Figures;

namespace BehavioralDesignPatterns.Visitors
{
   public class VisitorPoint : IVisitor
    {
        public string Visit(JsonSerialize serializer, object parms)
        {
            Point p = parms as Point;
            return $"{{\"X\":{p.X},\"Y\":{p.Y}}}";
        }

        public string Visit(XmlSerialize serializer, object parms)
        {
            Point c = parms as Point;
            XElement res = new XElement("Point",
                new XElement("X", c.X),
                new XElement("Y", c.Y)
            );
            return res.ToString();
        }
    }
}
