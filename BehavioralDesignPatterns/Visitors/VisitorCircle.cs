﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using BehavioralDesignPatterns.Strategy;
using BehavioralDesignPatterns.Visitors.Figures;

namespace BehavioralDesignPatterns.Visitors
{
    public class VisitorCircle : IVisitor
    {
        public string Visit(JsonSerialize serializer, object parms)
        {
            Circle c = parms as Circle;
            return $"{{\"Radius\":{c.Radius}}}";
        }

        public string Visit(XmlSerialize serializer, object parms)
        {
            Circle c = parms as Circle;
            XElement res = new XElement("Circle",
                new XElement("Radius", c.Radius)
            );
            return res.ToString();
        }
    }
}
