﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;
using BehavioralDesignPatterns.Strategy;
using BehavioralDesignPatterns.Visitors.Figures;

namespace BehavioralDesignPatterns.Visitors
{
    public class VisitorSquare : IVisitor
    {
        public string Visit(JsonSerialize serializer, object parms)
        {
            Square s = parms as Square;
            return $"{{\"Side\":{s.Side}}}";
        }

        public string Visit(XmlSerialize serializer, object parms)
        {
            Square s = parms as Square;
            XElement res = new XElement("Square",
                new XElement("Side", s.Side)
            );
            return res.ToString();
        }
    }
}
