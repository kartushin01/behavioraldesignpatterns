﻿using BehavioralDesignPatterns.Strategy;

namespace BehavioralDesignPatterns.Visitors
{
    public interface IVisitor
    {
        string Visit(JsonSerialize serializer, object parms);

        string Visit(XmlSerialize serializer, object parms);

    }
}
