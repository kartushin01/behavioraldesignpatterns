﻿using BehavioralDesignPatterns.Visitors;
using BehavioralDesignPatterns.Visitors.Figures;

namespace BehavioralDesignPatterns.Strategy
{
    public class Request
    {
        public readonly string _operation;
        public string _visitor;
        public IShape _data;
        public Request(string operation, string visitor, IShape data)
        {
            _operation = operation;
            _visitor = visitor;
            _data = data;
        }

    }

}
