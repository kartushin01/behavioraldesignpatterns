﻿using BehavioralDesignPatterns.Visitors;

namespace BehavioralDesignPatterns.Strategy
{
    public class JsonSerialize : IStrategy
    {
        public object _data;
        public string Execute(IVisitor visitor, Request request)
        {
            _data = request._data;
            return visitor.Visit(this, _data);
        }
    }
}
