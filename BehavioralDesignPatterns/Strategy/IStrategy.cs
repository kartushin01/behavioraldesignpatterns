﻿using BehavioralDesignPatterns.Visitors;

namespace BehavioralDesignPatterns.Strategy
{
    public interface IStrategy
    {
        string Execute(IVisitor visitor, Request request);
    }
}
