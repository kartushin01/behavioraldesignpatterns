﻿using BehavioralDesignPatterns.Visitors;
using System;

namespace BehavioralDesignPatterns.Strategy
{
    class Context
    {
        private IStrategy _strategy;
        private IVisitor _visitor;

        public void SetStrategy(Request request)
        {
            var s = Activator.CreateInstance(null,
                "BehavioralDesignPatterns.Strategy." + request._operation);

            _strategy = (IStrategy)s.Unwrap();
        }

        public void SetVisitor(Request request)
        {
            var v = Activator.CreateInstance(null,
                "BehavioralDesignPatterns.Visitors." + request._visitor);
            _visitor = (IVisitor)v.Unwrap();
        }

        public string Serialize(Request request)
        {
            SetStrategy(request);
            SetVisitor(request);
            return _strategy.Execute(_visitor, request);
        }
    }
}
